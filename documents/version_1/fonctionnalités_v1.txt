* Fonctionnalit�s version 1

- fonctionnalit� 1 : G�n�rer un labyrinthe
- descriptif texte : un labyrinthe contient des cases, il a pour dimension 10x10 et poss�de une entr�e en (5;0), son contour n'est pas traversable
- crit�res de validations :
	- crit�re 1 : l'entr�e doit �tre � l'endroit pr�vu 
	- crit�re 2 : le contour ne doit pas �tre traversable
- responsable : widehem1u
- diagramme de sequence : Diagramme de s�quence labyrinthe et case.png
- explication textuelle fonctionnement : cr�aton d'un labyrinthe initial de 10 cases sur 10 cases entour� d'un mur avec une entr�e en (5,0) et enti�reent vide � l'int�rieur




- fonctionnalit� 2 : G�n�rer une case
- descriptif texte : cr�er une case et indique si elle est traversable ou non 
- crit�res de validations :
	- crit�re 1 : savoir si la case est occupable ou non
- responsable : widehem1u
- diagramme de sequence : Diagramme de s�quence labyrinthe et case.png
- explication textuelle fonctionnement : cr�ation d'un case traversable ou non et sans entit� dessus



- fonctionnalit� 3 : G�n�rer un personnage
- descriptif texte : un personnage poss�de un nom et des points de vie, il poss�de aussi une position
- crit�res de validations :
	- crit�re 1 : le personnage ne peut pas vivre avec moins de 0 pv
	- crit�re 2 : ne peut pas traverser les murs
- responsable : maucci4u
- diagramme de sequence : 
- explication textuelle fonctionnement : creer un personnage g�n�rique ou un param�trable ainsi que de savoir si il est vivant ou mort





- fonctionnalit� 4 : Interface entit�
- descriptif texte : une interface entit� repr�sente un ensemble d'objets qui peuvent �tre utilis� et intervenir
- crit�res de validations :
	- crit�re 1 : 
	- crit�re 2 :
- responsable : busca5u
- diagramme de sequence : 
- explication textuelle fonctionnement :



- fonctionnalit� 5 : G�rer les d�placement
- descriptif texte : D�placement spatial dans un univers 2D.
- crit�res de validations :
 	- crit�re 1 : D�placement possible uniquement dans des cases �vide�.
 	- crit�re 2 : D�placement horizontal et vertical.
- responsable : drommer3u
- diagramme de sequence : Diagramme_Sequence_seDeplacer
- explication textuelle fonctionnement : la m�thode prends comme param�tre un charactere, si ce caractere est N,E,S ou W le mouvement se fait respectivement vers le haut, la droite, le bas ou la gauche




- fonctionnalit� 6 : Une case peut �tre occup�e
- descriptif texte :  une case peut contenir un mur, un personnage ou tout type d�objet
- crit�res de validations :
 	- crit�re 1 : Une case n�est pas un mur, la case doit �tre vide.
 	- crit�re 2 :, Une case ne contient pas de personnage ni de monstre
- responsable : widehem1u
- diagramme de sequence : 
- explication textuelle fonctionnement :



- fonctionnalit� 7 : Cr�ation de Monstre semblable au aventuriers
- descriptif texte : Une entit�e capable d�interagir hostilement avec le joueur.
- crit�res de validations :
	- crit�re 1 : n�existe qu�avec une vie positive
 	- crit�re 2 : ne peut pas se trouver hors du labyrinthe
- responsable : bernet13u
- diagramme de sequence : 
- explication textuelle fonctionnement :

