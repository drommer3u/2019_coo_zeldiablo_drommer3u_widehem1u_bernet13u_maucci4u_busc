package tests;
import org.junit.Test;

import modele.Labyrinthe;
import modele.Monstre;
import modele.Niveau;
import modele.Personnage;

import static org.junit.Assert.*;

public class TestPersonnage {
	/**
	 * test getter nom vide
	 */
    @Test
    public void getNomVide() {
    	Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
        Personnage personnage = new Personnage(l);
        assertEquals("Nom du constructeur vide","Steve",personnage.getNom());
    }
    /**
     * test getter nom
     */
    @Test
    public void getNom() {
    	Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
        Personnage personnage = new Personnage("Jean",20,5,5,l);
        assertEquals("Nom du constructeur","Jean",personnage.getNom());
    }
    
    /**
     * test getter pv vide
     */
    @Test
    public void getPvVide() {
    	Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
        Personnage personnage = new Personnage(l);
        assertEquals("Pv du constructeur vide",5,personnage.getPv());
    }

    /**
     * test getter pv
     */
    @Test
    public void getPv() {
    	Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
        Personnage personnage = new Personnage("Jean",20,5,5,l);
        assertEquals("Pv du constructeur",20,personnage.getPv());
    }
    /**
     * test getter abscisse vide
     */
    @Test
    public void getPosXVide() {
    	Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
        Personnage personnage = new Personnage(l);
        assertEquals("Pos X du constructeur vide",1,personnage.getPosX());
    }
    /**
     * test getter abscisse 
     */
    @Test
    public void getPosX() {
    	Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
        Personnage personnage = new Personnage("Jean",20,5,5,l);
        assertEquals("Pos X du constructeur",5,personnage.getPosX());
    }

    /**
     * test getter ordonnee vide
     */
    @Test
    public void getPosYVide() {
    	Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
        Personnage personnage = new Personnage(l);
        assertEquals("Pos Y du constructeur vide",1,personnage.getPosY());
    }
    /**
     * test getter ordonnee vide
     */
    @Test
    public void getPosY() {
    	Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
        Personnage personnage = new Personnage("Jean",20,5,5,l);
        assertEquals("Pos Y du constructeur",5,personnage.getPosY());
    }

    /**
     * test deplacement sud
     */
    @Test
    public void seDeplacerSud() {
    	Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
        Personnage personnage = new Personnage("Jean",20,5,5,l);
        personnage.seDeplacer("S");
        assertEquals("on devrait avoir 4 en Y", 4,personnage.getPosY());
    }
    /**
     * test deplacement nord
     */
    @Test
    public void seDeplacerNord() {
    	Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
        Personnage personnage = new Personnage("Jean",20,5,5,l);
        personnage.seDeplacer("N");
        assertEquals("on devrait avoir 6 en Y", 6,personnage.getPosY());
    }
    /**
     * test deplacement est
     */
    @Test
    public void seDeplacerEst() {
    	Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
        Personnage personnage = new Personnage("Jean",20,5,5,l);
        personnage.seDeplacer("E");
        assertEquals("on devrait avoir 6 en X", 6,personnage.getPosX());
    }
    /**
     * test deplacement ouest
     */
    @Test
    public void seDeplacerOuest() {
    	Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
        Personnage personnage = new Personnage("Jean",20,5,5,l);
        personnage.seDeplacer("O");
        assertEquals("on devrait avoir 4 en X", 4,personnage.getPosX());
    }
    /**
     * test attaque
     */
	@Test
	public void testAttaquer() {
		Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
		Monstre bill = new Monstre("bill",10,2,2,l);
		Personnage personnage = new Personnage("Jean",20,3,2,l);
		personnage.attaquer();
		assertEquals("attaque non porter",bill.getPv(),9);
	}
	

}