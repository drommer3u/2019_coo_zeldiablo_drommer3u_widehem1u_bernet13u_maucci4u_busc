package tests;
import modele.Labyrinthe;
import modele.Niveau;
import modele.Personnage;
import org.junit.Test;

import modele.Case;
import modele.Labyrinthe;
import modele.Personnage;

import static org.junit.Assert.*;

public class CaseTest {
	/**
	 * test si la case est traversable ou non
	 */
    @Test
    public void isTraversable() {
        Case c = new Case(true);
        boolean traversable=c.isTraversable();
        Case c2 = new Case(false);
        boolean traversable2=c2.isTraversable();

        assertEquals("la case devrait être traversable",true,traversable);
        assertEquals("la case ne devrait pas être traversable",false,traversable2);
    }
    /**
     * test si la case est occupee
     */
    @Test
    public void isOccupe() {
    	Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
        Case c = new Case(true);
        Case c2 = new Case(true);
        Personnage p= new Personnage(lab);
        c2.placerEntite(p);

        assertEquals("la case ne devrait pas être occupe",false,c.isOccupe());
        assertEquals("la case devrait être occupee",true,c2.isOccupe());

    }
    /**
     * test du placement dentite sur une case
     */
    @Test
    public void PlacerEntite() {
    	Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
        Case c = new Case(true);
        Personnage p= new Personnage(lab);
        c.placerEntite(p);

        assertEquals("la case devrait être occupe",true,c.isOccupe());
        assertEquals("on devrait avoir le personnage sur la case",p,c.getEntitesCase());
    }
    /**
     * test retirer entite dune case
     */
    @Test
    public void RetirerEntite() {
    	Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
        Case c = new Case(true);
        Case c2 = new Case(true);
        Personnage p= new Personnage(lab);
        c2.placerEntite(p);
        c2.retirerEntite();

        assertEquals("la case ne devrait plus être occupe",false,c.isOccupe());
        assertEquals("on ne devrait plus avoir le personnage sur la case",null,c.getEntitesCase());
    }
}