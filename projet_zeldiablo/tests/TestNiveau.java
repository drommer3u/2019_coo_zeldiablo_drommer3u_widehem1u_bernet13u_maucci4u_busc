package tests;
import static org.junit.Assert.*;

import org.junit.Test;

import modele.Labyrinthe;
import modele.Niveau;

public class TestNiveau {
	/**
	 * test constructeur
	 */
	@Test
	public void testConstructeur() {
		String s = "test";
		Niveau n = new Niveau(s);
		
		assertEquals("le nom du niveau devrait �tre test",s,n.getNom());
	
	}
	/**
	 * test ajouter et retirer labyrinthe dun niveau
	 */
	@Test
	public void testAjouterRetirerLabyrinthe() {
		String s = "test";
		Niveau n = new Niveau(s);
        Labyrinthe lab=new Labyrinthe(n);
		Labyrinthe l2 = new Labyrinthe(n);
		n.ajouterLab(lab);
		n.ajouterLab(l2);
		assertEquals("le labyrinthe devrait �tre ajout� a la liste",lab,n.getLabs().get(0));
		assertEquals("la taille de la liste devrait �tre de 2",2,n.getLabs().size());
		n.retirerLab(l2);
		assertEquals("la taille de la liste devrait �tre de 1",1,n.getLabs().size());
	}
	

}
