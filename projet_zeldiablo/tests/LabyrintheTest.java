package tests;
import static org.junit.Assert.*;

import modele.Case;
import modele.Labyrinthe;
import modele.Niveau;
import modele.Personnage;

public class LabyrintheTest {
	/**
	 * test getter tableau case
	 */
    @org.junit.Test
    public void getCases() {
    	Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
        Case[][] cases = lab.getCases();


        assertEquals("la taille devrait être de 10",10,cases.length);
        assertEquals("la taille devrait être de 10",10,cases[0].length);
        assertEquals("l'entree devrait être en (5,0)",true,cases[5][0].isTraversable());
    }
    /**
	 * test getter case
	 */
    @org.junit.Test
    public void getCase() {
    	Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
        Personnage p = new Personnage(lab);
        Case c1 = lab.getCase(0,0);
        Case c2= lab.getCase(1,1);
        Case c3 = lab.getCase(2,2);


        assertEquals("la case devrait etre non traversable",false,c1.isTraversable());
        assertEquals("la taille devrait être occupée",true,c2.isOccupe());
        assertEquals("la case devrait etre traversable",true,c3.isTraversable());
        assertEquals("la taille ne devrait pas être occupée",false,c3.isOccupe());
    }
    /**
	 * test getter taille abscisse 
	 */
    @org.junit.Test
    public void getTailleX() {
    	Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
        int tX = lab.getTailleX();

        assertEquals("la taille devrait être de 10",10,tX);
    }
    /**
	 * test getter taille ordonnee 
	 */
    @org.junit.Test
    public void getTailleY() {
    	Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
        int tY = lab.getTailleY();

        assertEquals("la taille devrait être de 10",10,tY);
    }
    /**
	 * test ajouter entite 
	 */
    @org.junit.Test
    public void ajouterEntite() {
    	Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
        Personnage p = new Personnage(lab);

        assertEquals("le labyrinthe devrait avoir une entite personnage",p,lab.getEntitesLab().get(0));
    }
}