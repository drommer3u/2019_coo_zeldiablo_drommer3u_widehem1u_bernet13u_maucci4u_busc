package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import modele.Amulette;
import modele.Labyrinthe;
import modele.Niveau;
import modele.Personnage;

public class TestAmulette {
	/**
	 * test du constructeur d'amulette, verifie si le nom est bien modifie
	 */
	@Test
	public void testConstructeur() {
		Niveau n = new Niveau("test");
		Labyrinthe l = new Labyrinthe(n);
		Amulette a = new Amulette("test",l);
		
		assertEquals("l'amulette cree devrait se nommer autrement", "test" , a.getNom());
	}
	
	/**
	 * test apres ajout d'amulette au labyrinthe, verifie si l'amulette si trouve bien
	 */
	@Test
	public void testajoutLabyrinthe() {
		Niveau n = new Niveau("test");
		Labyrinthe l = new Labyrinthe(n);
		Amulette a = new Amulette("test",l);
		l.ajouterItem(a, 2, 2);
		assertEquals("une amulette devrait se trouver dans le labyrinthe", a,l.getItemLab().get(0));
	}
	
	/**
	 * test apres recuperation dune amulette par un joueur
	 */
	@Test
	public void testrecupererAmulette() {
		Niveau n = new Niveau("test");
		Labyrinthe l = new Labyrinthe(n);
		Amulette a = new Amulette("test",l);
		Personnage p = new Personnage("nom",5, 5,5,l);
		l.ajouterItem(a,5,5);
		assertEquals("le personnage devrait porter une amulette", a ,p.getItems().get(0));
	}
	
	

}
