package tests;

import org.junit.Test;

import modele.Labyrinthe;
import modele.Niveau;
import modele.Personnage;
import modele.Projectile;

import static org.junit.Assert.*;

public class ProjectileTest {

	/**
	 * test constructeur projectile
	 */
    @Test
    public void constructeur() {
    	Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
        Personnage p=new Personnage(lab);
        Projectile fleche = new Projectile(p,"fleche");
        assertEquals("la direction devrait etre N", "N", fleche.getDirection());
        assertEquals("l'abcisse devrait etre 5",5,fleche.getPosX());
        assertEquals("l'ordonee devrait etre 1",1,fleche.getPosY());
    }

    /**
     * test deplacement projectile
     */
    @Test
    public void seDeplacer() {
    	Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
        Personnage p=new Personnage(lab);
        Projectile fleche = new Projectile(p,"fleche");
        fleche.seDeplacer();
        assertEquals("l'abcisse devrait etre 5",5,fleche.getPosX());
        assertEquals("l'ordonee devrait etre 3",3,fleche.getPosY());
    }
}