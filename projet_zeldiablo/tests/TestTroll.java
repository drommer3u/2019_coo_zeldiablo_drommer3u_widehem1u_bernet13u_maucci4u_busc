package tests;
import static org.junit.Assert.*;

import org.junit.Test;
import modele.Labyrinthe;
import modele.Niveau;
import modele.Troll;


public class TestTroll {
	/**
	 * test regeneration vie troll
	 */
	@Test
	public void TestRegeneration() {
		Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
		Troll Trundle = new Troll("Trundle",4,3,3,l);
		Trundle.prendreDegat();
		Trundle.seDeplacer("N");
		Trundle.seDeplacer("N");
		assertEquals("devrait etre Soigne",Trundle.getPv(),4);
	}
	/**
	 * test regeneration vie pleine
	 */
	@Test
	public void TestRegenerationFullVie() {
		Niveau n = new Niveau("test");
        Labyrinthe l=new Labyrinthe(n);
		Troll Trundle = new Troll("Trundle",4,3,3,l);
		Trundle.seDeplacer("O");
		assertEquals("ne devrait pas etre Soigne",Trundle.getPv(),4);
	}
	

}
