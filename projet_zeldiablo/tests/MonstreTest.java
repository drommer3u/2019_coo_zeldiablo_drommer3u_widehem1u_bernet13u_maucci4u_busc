package tests;
import static org.junit.Assert.*;

import org.junit.Test;

import modele.Labyrinthe;
import modele.Monstre;
import modele.Niveau;
import modele.Personnage;

public class MonstreTest {
	/**
	 * test constructeur vide
	 */
	@Test
	public void testConstructeurVide() {
		Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
		Monstre bertrand = new Monstre(lab);
		assertEquals("devrait etre Monstre",bertrand.getNom(),"Monstre");
		assertEquals("X devrait etre 0",bertrand.getPosX(),0);
		assertEquals("Y devrait etre 0",bertrand.getPosY(),0);
	}
	/**
	 * test constructeur avec parametres
	 */
	@Test
	public void testConstructeurParam() {
		Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
		Monstre bill = new Monstre("bill",10,2,2,lab);
		assertEquals("devrait etre bill",bill.getNom(),"bill");
		assertEquals("X devrait etre 2",bill.getPosX(),2);
		assertEquals("Y devrait etre 2",bill.getPosY(),2);
	}
	
	/**
	 * test attaque
	 */
	@Test
	public void testAttaquer() {
		Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
		Monstre bill = new Monstre("bill",10,2,2,lab);
		Personnage personnage = new Personnage("Jean",20,3,2,lab);
		bill.attaquer();
		assertEquals("attaque non porter",personnage.getPv(),19);
	}
	/**
	 * test a portee
	 */
	@Test
	public void testAporter() {
		Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
		Monstre bill = new Monstre("bill",10,2,2,lab);
		Personnage personnage = new Personnage("Jean",20,3,2,lab);
		assertEquals("detetction fausse",bill.aPorter(),true);
	}
	
	/**
	 * test indice
	 */
	@Test
	public void testIndice() {
		Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
		Monstre bill = new Monstre("bill",10,2,2,lab);
		Personnage personnage = new Personnage("Jean",20,3,2,lab);
		assertEquals("Indice faux",bill.getIndicePersonnage(),1);
	}
	
	/**
	 * test deplacement aleatoire
	 */
	@Test
	public void SeDeplacerAlea() {
		Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
		Monstre bill = new Monstre("bill",10,2,2,lab);
		int x= bill.getPosX();
		int y = bill.getPosY();
		bill.seDeplacerAlea();
		assertEquals("le monstre devrait avoir bougé",true,(bill.getPosX()!=x||bill.getPosY()!=y));
	}
	/**
	 * test mort
	 */
	@Test
	public void mourir() {
		Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
		Monstre bill = new Monstre("bill",1,2,2,lab);
		bill.prendreDegat();
		assertEquals("le monstre devrait etre mort",true,bill.estMort());
	}
}
