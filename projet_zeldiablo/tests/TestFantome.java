package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import modele.Labyrinthe;
import modele.Niveau;
import modele.Fantome;

public class TestFantome {
	/**
	 * test deplacement aleatoire
	 */
	@Test
	public void SeDeplacerAlea() {
		Niveau n = new Niveau("test");
        Labyrinthe lab=new Labyrinthe(n);
		Fantome casper = new Fantome("casper",10,2,2,lab);
		int x= casper.getPosX();
		int y = casper.getPosY();
		casper.seDeplacerAlea();
		assertEquals("le monstre devrait avoir bouger",true,(casper.getPosX()!=x||casper.getPosY()!=y));
	}
}
