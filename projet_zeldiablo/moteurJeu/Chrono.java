package moteurJeu;

public class Chrono {
	private double tD;
	private double tA;
	private double tInferieur;
	private double tSuperieur;
	
	public Chrono(double tD) {
		this.tD = tD*1000;
		this.tA = 0;
		this.tInferieur = tD*0.85;
		this.tSuperieur = tD*1.15;

	}

	public boolean check() {
		double temps = System.currentTimeMillis() - tA;
		if (temps > tD ) {
			this.refresh();
			return true;
		} else 
			return false;
	}
	
	private void refresh() {
		tA = System.currentTimeMillis();
	}
	
}
