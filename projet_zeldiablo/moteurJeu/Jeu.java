package moteurJeu;

import java.awt.Graphics;
import java.awt.event.KeyEvent;

import modele.Labyrinthe;
import modele.Niveau;

public class Jeu implements JeuAbstract{
	private Dessin des;
	private Niveau nPrinc; //niveau
	private CClavier clav;
	private CSouris sou;
	private boolean fin;

	//peut avoir attribut parametre avec les touches bind etc
	public Jeu(Dessin des, Niveau n) {
		this.des = des;
		this.nPrinc = n;
		this.fin = false;
		n.setJeu(this);
	}
	
	public Jeu(Dessin des) {
		this.des = des;
		this.fin = false;

	}

	public String evoluer(CClavier clavier, CSouris souris) {
		if(!etreFini()) {
			this.clav = clavier;
			this.sou = souris;
			contDep();
			contAtt();
			nPrinc.getLabEc().refresh();
		}
			else {
				System.out.println("fini");
			}
			return "";
	}
	
	/**
	 * m�thode Controle d�placement
	 */
	public void contDep() {
		//peut prendre les bind attribut de parametre
		if (clav.getTyped(KeyEvent.VK_UP)) {
			des.getLab().getPers().seDeplacer("N");
		} else if (clav.getTyped(KeyEvent.VK_LEFT)) {
			des.getLab().getPers().seDeplacer("O");
		} else if (clav.getTyped(KeyEvent.VK_RIGHT)) {
			des.getLab().getPers().seDeplacer("E");
		} else if (clav.getTyped(KeyEvent.VK_DOWN)) {
			des.getLab().getPers().seDeplacer("S");
		}
	}


	/**
	 * m�thode Controle des attaques
	 */
	public void contAtt() {
		//peut prendre les bind attribut de parametre
		if (clav.getTyped(KeyEvent.VK_SPACE)) {
			des.getLab().getPers().attaquer();
		} else if (clav.getTyped(KeyEvent.VK_F)) {
			des.getLab().getPers().attaqueDistance();
			System.out.println("bluchi��");
		}
	}

	public boolean etreFini() {
		if(des.getLab().getPers().estMort() || fin) {
			return true;
		} else {
			return false;
		}
	}
	
	public void setFin() {
		this.fin = true;
	}
	
	/*public void setLab(Labyrinthe l) {

	}*/
}
