package moteurJeu;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import java.io.File;

public class Musique {
	

	public Musique() {
		
	}
		

	public static synchronized void lancerMusique(String musique) {
		new Thread(() -> {
			try {
				AudioInputStream audio = AudioSystem.getAudioInputStream(new File("projet_zeldiablo\\musiques\\"+musique+".wav"));
				Clip clip = AudioSystem.getClip();
				clip.open(audio);
				clip.loop(Clip.LOOP_CONTINUOUSLY);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();}
}


