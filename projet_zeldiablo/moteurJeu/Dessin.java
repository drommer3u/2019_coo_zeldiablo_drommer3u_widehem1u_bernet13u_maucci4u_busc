package moteurJeu;

import java.awt.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import sprite.Sprites;

import modele.Case;
import modele.Labyrinthe;
import modele.Niveau;
import modele.Personnage;


public class Dessin implements DessinAbstract {
	private int y;
	private int x;
	private Labyrinthe lab;
	private boolean ex;
	/**
	 * Constructeur du dessin
	 * @param lab labyrinthe � afficher
	 */
	public Dessin(Labyrinthe lab) {
		super();
		this.lab = lab;
		chargerIm();

	}
	
	public Dessin(Niveau n) {
		super();
		this.lab = n.getLabEc();
		chargerIm();

	}
	
	public void chargerIm() {
		Sprites.chargerImage("fin","projet_zeldiablo\\images\\fin.png");
		Sprites.chargerImage("sol","projet_zeldiablo\\images\\dirtF.png");
		Sprites.chargerImage("mur","projet_zeldiablo\\images\\solF.png");
		Sprites.chargerImage("fond","projet_zeldiablo\\images\\fond2.png");
		Sprites.chargerImage("piege2","projet_zeldiablo\\images\\piege2.png");
		Sprites.chargerFeuille("troll","projet_zeldiablo\\images\\trollF.png",3,4);
		Sprites.chargerFeuille("ghost","projet_zeldiablo\\images\\ghost.png",5,1);
		Sprites.chargerFeuille("arrow","projet_zeldiablo\\images\\arrow.png",4,1);
		Sprites.chargerFeuille("escalier","projet_zeldiablo\\images\\escalier.png",4,1);
		Sprites.chargerFeuille("piege","projet_zeldiablo\\images\\piege.png",4,2);
		Sprites.chargerFeuille("hero","projet_zeldiablo\\images\\hero.png",6,4);
		Sprites.chargerFeuille("amulette","projet_zeldiablo\\images\\amulette.png",10,1);
		Sprites.chargerFeuille("boss","projet_zeldiablo\\images\\bossFFF.png",1,4);
		
		//Sprites.chargerImage("sol","solF.jpg");
	}
	
	
	public void dessiner(BufferedImage image) {
		Graphics g = image.getGraphics();
		Sprites.dessiner(g, "fond", 0, 0);
		affLab(g);
		g.dispose();
	}
	
	
	
	public void affLab(Graphics g) {
		Case[][] tabLab = lab.getCases();
		int px = 0;
		int py = 0;
		for(int y = this.lab.getTailleY()-1; y >= 0;y--) {
			px = 0;
			for(int x = 0; x < this.lab.getTailleX();x++) { 
				tabLab[x][y].affiche(g,px,py);
				px += 64;
			}
			py += 64;
		}
	}
	
	public boolean testRond(Graphics g, boolean t) {
		if (t) {
			g.fillOval(0, 0, 50, 50);
			return false;
		} else {
			g.fillOval(0, 0, 500, 500);
			return true;
		}
		//pour test : boolean test = testRond(g,test)
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public Labyrinthe getLab() {
		return this.lab;
	}
	
	public void setLab(Labyrinthe lab) {
		this.lab = lab;
	}
	

}
