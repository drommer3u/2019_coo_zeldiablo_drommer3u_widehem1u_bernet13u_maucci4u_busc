package modele;

import java.awt.Graphics;

public interface Entite {
	
	public String getNom();
	public int getPv();
	public int getPosX();
	public int getPosY();
	public String getDirection();
	public Labyrinthe getLab();
	public void seDeplacer(String s);
	public void prendreDegat();
	public void prendreDegatPiege();
	public void attaquer();
	public void setRandPos();
	public boolean estMort();
	public void afficher(Graphics g, int px, int py);
	public int distanceEntre();

}
