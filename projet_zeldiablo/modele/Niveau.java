package modele;
import java.util.ArrayList;

import moteurJeu.Dessin;
import moteurJeu.Jeu;

public class Niveau {
	/**
	 * attribut liste de labyrinthe represente les niveaux disponibles
	 */
	private ArrayList<Labyrinthe> labs;
	/**
	 * attribut nom represente le nom du niveau, permettra de la lancer par le biais d'un jeu
	 */
	private String nom;
	/**
	 * indique le labyrinthe en cours
	 */

	private Labyrinthe labEnCours;
	
	/**
	 * jeu auquel le niveau est lie
	 */
	private Jeu j;
	
	/**
	 * personnage hero du niveau
	 */
	private Personnage hero;
	
	/**
	 * dessin lie au niveau
	 */
	private Dessin d;
	
	
	
	
	
	/**
	 * constructeur de niveau, initialise le nom et la liste de labyrinthe
	 * @param s nom du niveau
	 */
	
	public Niveau(String s, Jeu j, Personnage p) {
		this.nom = s;
		this.j = j;
		this.labs = new ArrayList<Labyrinthe>();
		this.ajouterLab(new Labyrinthe("laby1.txt"));
		this.ajouterLab(new Labyrinthe("laby2.txt"));
		this.hero = p;
	}
	/**
	 * cosntructeur
	 * @param s nom du niveau
	 * @param j jeu lie au niveau
	 */
	public Niveau(String s, Jeu j) {
		this.nom = s;
		this.j = j;
		this.labs = new ArrayList<Labyrinthe>();
		this.ajouterLab(new Labyrinthe("laby1.txt"));
		this.ajouterLab(new Labyrinthe("laby2.txt"));
	}
	/**
	 * constructeur
	 * @param s nom du niveau
	 */
	public Niveau(String s) {
		this.nom = s;
		this.labs = new ArrayList<Labyrinthe>();
		this.ajouterLab(new Labyrinthe("laby1.txt"));
		this.ajouterLab(new Labyrinthe("laby2.txt"));
		this.ajouterLab(new Labyrinthe("laby3.txt"));
		this.ajouterLab(new Labyrinthe("laby4.txt"));
		/////lab
		
		for (int i = 0;i < labs.size()-1; i++) {
			
			for (int j = 0; j < 5; j++) {
				new Troll(this.labs.get(i));
			}
			for (int k = 0; k < 2; k ++) {
				new Fantome(this.labs.get(i));
			}

		}
		
		new Boss(this.labs.get(2));
			
		this.labEnCours = labs.get(0);
	}
	
	/**
	 * permet d'ajouter un labyrinthe a la liste
	 * @param l labyrinthe a ajouter
	 */
	public void ajouterLab(Labyrinthe l) {
		labs.add(l);
		l.setNiveau(this);
	}
	/**
	 * permet de retirer un labyrinthe de la liste
	 * @param l labyrinthe a retirer
	 */
	public void retirerLab(Labyrinthe l) {
		int i = labs.indexOf(l);
		labs.remove(i);
	}
	
	/**
	 * retourne le nom du niveau
	 * @return nom du niveau
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * getter tableau labyrinthe
	 * @return labs tableau de labyrinthe
	 */
	public ArrayList<Labyrinthe> getLabs(){
		return this.labs;
	}
	/**
	 * indique le labyrinthe suivant
	 * @return labyrinthe suivant
	 */
	public Labyrinthe suivant(){
		try{
			int i =labs.indexOf(labEnCours);
			return labs.get(i+1);}
		catch(Exception e){
			e.printStackTrace();
			Labyrinthe lab = null;
			return lab;
		}
	}
	public void refresh() {
		// if checkamuellte this.j.setFin
		
	}
	/**
	 * getter
	 * @return labyrinthe en cours
	 */
	public Labyrinthe getLabEc() {
		return this.labEnCours;
	}
	/**
	 * setter
	 * @param p personnage
	 */
	public void setPers(Personnage p) {
		this.hero = p;
	}
	/**
	 * setter
	 * @param j jeu
	 */
	public void setJeu(Jeu j) {
		this.j = j;
	}
	/**
	 * permet de passer au labyrinthe suivant
	 */
	public void labSuivant() {
		if (this.labs.get(( this.labs.indexOf(labEnCours) + 1 )) != null) {
			this.labEnCours = this.labs.get(( this.labs.indexOf(labEnCours) + 1 ));
			this.hero.setLab(labEnCours);
			this.d.setLab(labEnCours);
			this.labEnCours.ajouterEntite(hero);
		}
	}
	/**
	 * setter
	 * @param d dessin
	 */
	public void setDess(Dessin d) {
		this.d = d;
	}
	
	public void setFin() {
		this.j.setFin();
	}
}
