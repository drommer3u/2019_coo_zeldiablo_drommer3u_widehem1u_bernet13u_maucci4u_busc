package modele;

import java.awt.Color;
import java.awt.Graphics;

import sprite.Sprites;

public class Case {
	/**
	 * booleen indiquant si la case est un mur ou non
	 */
    protected boolean traversable;
    /**
     * booleen indiquand si la case est occupee par une entite
     */
    protected boolean estOccupe;
    /**
     * indique si la case possede un item
     */
    protected boolean possedeItem;
    /**
     * indique l'entite contenu par la case
     */
    protected Entite entitesCase;
    /**
     * indique le projectile contenu par la case
     */
    protected Projectile projectileCase;
    /**
     * indique l'item contenu par la case
     */
    protected Item itemCase;
    
 
    /**
     * cosntructeur de case
     * @param travers indique que la case est libre
     */
    public Case(boolean travers){
        this.traversable=travers;
        this.estOccupe=false;
        this.possedeItem = false;
        this.entitesCase = null;
        this.projectileCase = null;
        this.itemCase = null;
    }

    /**
     * permet de savoir si la case est occupee
     * @return case occupee ou non
     */
    public boolean isOccupe() {
        return estOccupe;
    }
    /**
     * indique si la case est traversable
     * @return
     */
    public boolean isTraversable() {
        return traversable;
    }

    	
    /**
     * getter entite
     * @return entite sur la case
     */
    public Entite getEntitesCase() {
        return entitesCase;
    }
    
    /**
     * setter entite
     * @param e entite a placer sur la case
     */
    public void placerEntite(Entite e){
        this.entitesCase=e;
        this.estOccupe=true;
    }
    /**
     * permet de retirer une entite dune case
     */
    public void retirerEntite(){
        this.entitesCase=null;
        this.estOccupe=false;
    }
    
    
    /**
     * setter Item
     * @param i item a placer 
     */
    public void placerItem(Item i){
        this.itemCase=i;
        this.possedeItem = true;
    }

    /**
     * permet de retirer un item de la case
     */
    public void retirerItem(){
        this.itemCase=null;
        this.possedeItem = false;
    }

    
    /**
     * permet de placer un projectile sur une case 
     * @param p
     */
    public void placerProjectile(Projectile p){
        this.projectileCase=p;
        this.estOccupe=true;
    }
    /**
     * permet de retirer le projectile de la case
     */
    public void retirerProjectile(){
        this.projectileCase=null;
        this.estOccupe=false;
    }
    /**
     * methode graphique
     * @param g parametre graphique
     * @param px position daffichage en abscisse
     * @param py position daffichage en ordonnee
     */
    public void affiche(Graphics g, int px, int py) {
    	//non entite = mur
    	
    	if (traversable) {
    		Sprites.dessiner(g, "sol", px, py);
    	} else if (!traversable){
    		Sprites.dessiner(g, "mur", px, py);
    	} if(this.possedeItem) {
    		this.itemCase.affiche(g, px, py);
    	} if(estOccupe && this.entitesCase != null) {
    		this.entitesCase.afficher(g, px, py);
    	} else {
    		if(estOccupe && this.projectileCase != null) {
    			projectileCase.afficher(g, px, py);

    			//this.entitesCase.afficher();
    		} else {
	            if (this.entitesCase != null) {
	                System.out.println("ici");
	                this.entitesCase.afficher(g, px, py);
	            }
    		}
    	}
    	
    }

    
    /**
     * getter projectile
     * @return projectile sur la case
     */
    public Projectile getProjectileCase() {
        return projectileCase;
    }

    
    /**
     * getter Item
     * @return item sur la case
     */
    public Item getItemCase() {
        return itemCase;
    }
}
