package modele;

import modele.Entite;
import modele.Labyrinthe;
import sprite.Sprites;
import moteurJeu.Chrono;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class Monstre implements Entite {
	/**
	 * nom du monstre
	 */
	protected String nom;
	/**
	 * points de vie du monstre
	 */
	protected int pv;
	/**
	 * abscisse du monstre
	 */
	protected int posX;
	/**
	 * ordonnee du monstre
	 */
	protected int posY;
	/**
	 * labyritnhe attribuer au monstre
	 */
	protected Labyrinthe lab;
	/**
	 * direction du monstre
	 */
	protected String direction;
	/**
	 * chronometre des actions du monstre
	 */
	protected Chrono cAction;
	/**
	 * indique si le monstre attaque ou non
	 */
	protected boolean attq;

	/**
	 * constructeur monstre
	 * @param l labyrinthe ou il se trouve
	 */
	public Monstre(Labyrinthe l) {
		this.nom = "Monstre";
		this.pv = 5;
		this.posX = 0;
		this.posY = 0;
		this.lab=l;
		this.lab.ajouterEntite(this);
		setChrono(0.5);
		direction = "N";
	}
	
	/**
	 * Constructeur avec parametre
	 * @param n nom du monstre
	 * @param vie vie du monstre
	 * @param posX abscisse de la position du monstre
	 * @param posY ordonnee de la position du monstre
	 */
	public Monstre(String n, int vie, int posX, int posY, Labyrinthe l) {
		this.nom = n;
		this.pv = vie;	
		this.posX = posX;
		this.posY = posY;
		this.lab=l;
		this.lab.ajouterEntite(this);
	}
	/**
	 * attribue un chronometre au monstre
	 * @param v duree avant chaque action
	 */
	public void setChrono(double v) {
		this.cAction = new Chrono(v);
	}
	
	/**
	 * retourne le nom
	 * @return retourne sous forme de string le nom du monstre
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * retourne la vie
	 * @return retourne la vie du monstre sous la forme d'un entier
	 */
	public int getPv() {
		return this.pv;
	}	
	
	/**
	 * retourne l'abscisse du monstre
	 * @return l'abscisse sous la forme d'un entier
	 */
	public int getPosX() {
		return this.posX;
	}
	
	/**
	 * retourne l'ordonnee du monstre
	 * @return l'ordonnee du monstre sous la forme d'un entie
	 */
	public int getPosY() {
		return this.posY;
}

	/**
	 * @return la direction du monstre
	 */
	@Override
	public String getDirection() {
		return direction;
	}

	/**
	 * @return le labyrinthe du personnage
	 */
	public Labyrinthe getLab() {
		return lab;
	}
	/**
	 * verifie letat du monstre
	 */
	public boolean estMort() {
		if (this.getPv() <= 0) {
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	 * methode qui permet de recevoir les degats d'un personnage
	 */
	public void prendreDegat() {
		this.pv= pv-1;
	}
	
	
	
	/**
	 * methode qui permet de frapper un personnage
	 */
	public void attaquer() {
		ArrayList<Entite> e =lab.getEntitesLab();
		if (aPorter()== true) {
			int index = this.getIndicePersonnage();
		    Entite cible =e.get(index);
		    cible.prendreDegat();
		}
	}
	
	
	
	/**
	 * attribue une position aleatoire
	 */
	public void setRandPos() {
		this.posX = (int)(Math.random()*(lab.getTailleX()));
		this.posY = (int)(Math.random()*(lab.getTailleY()));
	}
	
	
	
	/**
	 * retourne l'indice du personnage present dans la liste
	 * @return l'index du personnage
	 */
public int getIndicePersonnage() {
	int index=0; 
	ArrayList<Entite> e =lab.getEntitesLab();
	for (int i=0; i < e.size();i++) {
		if (e.get(i) instanceof Personnage) {
		index = i;
	}
}
	return index;
}

/**
 * indique si le monstre est proche ou non d'un personnage
 * @return l'information du status de proximite
 */
	public boolean aPorter() {
		boolean res=false;
		int index = this.getIndicePersonnage();
		ArrayList<Entite> e =lab.getEntitesLab();
		
		if (e.get(index).getPosX() == this.getPosX()+1 && e.get(index).getPosY() == this.getPosY()) {
		res = true;
	} else {
		if (e.get(index).getPosX() == this.getPosX()-1 && e.get(index).getPosY() == this.getPosY()) {
		res = true;
	} else {
		if (e.get(index).getPosX() == this.getPosX() && e.get(index).getPosY() == this.getPosY()+1) {
		res = true;
	} else { 
		if (e.get(index).getPosX() == this.getPosX() && e.get(index).getPosY() == this.getPosY()-1) {
		res = true;
	}
   }
  }
 }
		return res;
}
	
	
	
	
	/**
	 * permet au monstre de se deplacer
	 */
	public void seDeplacer(String s) {
		switch (s){
			
			case "N":
				direction = "N";
				if(this.posY!=this.getLab().getTailleY()-1) {
					if (this.lab.getCase(this.posX, this.posY + 1).isTraversable() && ! this.lab.getCase(this.posX, this.posY + 1).isOccupe()) {
						this.lab.getCase(this.posX, this.posY).retirerEntite();
						this.posY += 1;
						this.lab.getCase(this.posX, this.posY).placerEntite(this);
					}
				}
				break;
			case "E":
				direction = "E";
				if(this.posX!=this.getLab().getTailleX()-1) {
					if (this.lab.getCase(this.posX + 1, this.posY).isTraversable() && ! this.lab.getCase(this.posX + 1, this.posY).isOccupe()) {
						this.lab.getCase(this.posX, this.posY).retirerEntite();
						this.posX += 1;
						this.lab.getCase(this.posX, this.posY).placerEntite(this);
					}
					break;
				}
			case "S":
				direction = "S";
				if(this.posY!=0) {
					if (this.lab.getCase(this.posX, this.posY - 1).isTraversable() && ! this.lab.getCase(this.posX, this.posY - 1).isOccupe()) {
						this.lab.getCase(this.posX, this.posY).retirerEntite();
						this.posY -= 1;
						this.lab.getCase(this.posX, this.posY).placerEntite(this);
					}
				}
				break;
			case "O":
				direction = "O";
				if(this.posX!=0) {
					if (this.lab.getCase(this.posX - 1, this.posY).isTraversable() && ! this.lab.getCase(this.posX - 1, this.posY).isOccupe()) {
						this.lab.getCase(this.posX, this.posY).retirerEntite();
						this.posX -= 1;
						this.lab.getCase(this.posX, this.posY).placerEntite(this);
					}
				}
				break;
		}
		this.direction=s;
	}
	
	
	/**
	 * methode permettant de deplacer un monstre de maniere aleatoire
	 * cette methode utilise une methode de deplacement seDeplacer
	 */
	public void seDeplacerAlea() {
		int i;
		i = (int)(Math.random()*(4));
		switch(i) {
		case 0:
			seDeplacer("N");
			break;
		case 1:
			seDeplacer("E");
			break;
		case 2:
			seDeplacer("S");
			break;
		case 3:
			seDeplacer("O");
			break;
		}
	}

	/**
	 * ia permettant des mouvement aleatoire aux monstres
	 */
	public void Ia() {
		if(cAction.check()) {
			if(!estMort()){
				if(aPorter()){
					attaquer();
				}
				else{
					seDeplacerAlea();
				}
			}
		}
	}
	/**
	 * ia amelioree permet aux monstres de foncer sur le joueur
	 */
	public void iaAmeliorer() {
		if (cAction.check()) {
			if(!estMort()){
				if (aPorter()) {
					attaquer();
				} else {
				if(distanceEntre()<=6){
					seDeplacer(directionCourte());
				}
				else{
					seDeplacerAlea();
				}
			}
		}
	}
}
	/**
	 * methode permettant de calculer la distance entre deux entite
	 */
	public int distanceEntre() {
		int res = 0;
		int index = this.getIndicePersonnage();
		ArrayList<Entite> e =lab.getEntitesLab();
		res = (int) (Math.sqrt(Math.pow(this.getPosX()-e.get(index).getPosX(),2) + Math.pow(this.getPosY()-e.get(index).getPosY(),2)));
		return res;
	}
	/**
	 * permet de prevoir les mouvement les plus court
	 * @return chaine de caractere correspondant au mouvement 
	 */
	public String directionCourte() {
		String res ="";
		int index = this.getIndicePersonnage();
		ArrayList<Entite> e =lab.getEntitesLab();
		if (Math.sqrt(Math.pow(this.getPosX()-e.get(index).getPosX(),2)) > Math.sqrt(Math.pow(this.getPosY()-e.get(index).getPosY(),2))) {
			if (this.getPosX() > e.get(index).getPosX()) {
				res = "O";
			} else {
				res = "E";
			}
		} else {
			if (this.getPosY() > e.get(index).getPosY()) {
				res = "S";
			} else {
				res = "N";
			}
		}
		return res;
	}
	/**
	 * methode daffichage graphique
	 */
	public void afficher(Graphics g, int px, int py) {
		if (direction == "N")
			Sprites.dessiner(g, "troll_1_3", px, py);
		if (direction == "S")
			Sprites.dessiner(g, "troll_1_0", px, py);
		if (direction == "E")
			Sprites.dessiner(g, "troll_1_2", px, py);
		if (direction == "O")
			Sprites.dessiner(g, "troll_1_1", px, py);
		g.setColor(new Color(0,0,0));
		g.fillRect(px+8, py-10, 50, 5);
		g.setColor(new Color(128,16,16));
		g.fillRect(px+8, py-10, 10*this.pv, 5);
	}
	/**
	 * permet de subir des degats
	 */
	@Override
	public void prendreDegatPiege() {
		this.pv-=4;
		
	}


	
}

