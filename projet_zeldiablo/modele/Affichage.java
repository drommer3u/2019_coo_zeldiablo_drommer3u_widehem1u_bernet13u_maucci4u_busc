package modele;

public class Affichage {
	private Labyrinthe l;
	
	/**
	 * constructeur d'affichage, initialise un labyrinthe
	 * @param pl labyrinthe a afficher
	 */
	public Affichage(Labyrinthe pl) {
		this.l = pl;
	}
	/**
	 * methode permettant l'affichage console du labyrinthe
	 */
	public void show() {
		Case[][] tabLab = l.getCases();
		for(int y = this.l.getTailleY()-1; y >= 0;y--) {
			System.out.println("");
			for(int x = 0; x < this.l.getTailleX();x++) { 
				/*
				 * si la case est traversable et n'est pas occup�e on met un espace
				 */
				if (tabLab[x][y].isTraversable() && !(tabLab[x][y].isOccupe())) {
					System.out.print(" _ ");
				}
				/*
				 * si la case est traversable et est occup�e on met le personnage
				 */
				else if (tabLab[x][y].isTraversable() && (tabLab[x][y].isOccupe())) {
					System.out.print(" O ");
				}
				/*
				 * sinon on fait un mur
				 */
				else {
					System.out.print(" X ");
				}
			}
			
		}
	}

}
