package modele;

import java.awt.Graphics;

import sprite.Sprites;

public class Amulette implements Item{
	/**
	 * attribut de position x pour abscisse et y pour ordonnee
	 */
	private int posX,posY;
	
	/**
	 * Labyrinthe l indique dans quel labyrinthe l'amulette se trouve
	 */
	private Labyrinthe l;
	/**
	 * attribut nom permet d'identifier l'amulette
	 */
	private String nom;
	
	/**
	 * attribut personnage represente le proteur de l'item
	 */
	private Personnage porteur;
	
	/**
	 * constructeur d'amulette, initialise le nom
	 * @param nom indique le futur nom de l'amulette
	 */
	public Amulette(String nom, Labyrinthe l) {
		this.posX = 0;
		this.posY = 0;
		this.nom = nom;
		porteur = null;
		this.l = l;
		l.ajouterItem(this, posX, posY);
	}
	
	/**
	 * getter de la position en abscisse
	 */
	@Override
	public int getPosX() {
		return this.posX;
	}
	/**
	 * getter de la position en ordonnee
	 */
	@Override
	public int getPosY() {
		return this.posY;
	}
	/**
	 * getter du labyrinthe dans lequel l'amulette se trouve
	 */
	@Override
	public Labyrinthe getLab() {
		return l;
	}
	/**
	 * getter du nom de l'amulette
	 */
	@Override
	public String getNom() {
		return this.nom;
	}

	
	
	/**
	 * permet d'ajouter une amulette a un labyrinthe
	 * @param la cible
	 */

	@Override
	public Personnage getPorteur() {
		return this.porteur;
	}
	
	/**
	 * methode permettant d'attribuer un porteur a une amulette
	 */
	@Override
	public void setPorteur(Personnage p) {
		this.porteur = p;
		
	}
	
	/**
	 * methode permettant dafficher une amulette avec un sprite
	 */
	@Override
	public void affiche(Graphics g, int x, int y) {
		Sprites.dessiner(g,  "amulette_0_0", x, y);

	}
	/**
	 * methode permettant dattribuer une position aleatoire a une amulette
	 */
	public void setRandPos() {
		this.posX = (int)(Math.random()*(l.getTailleX()));
		this.posY = (int)(Math.random()*(l.getTailleY()));
	}
	
	
}
