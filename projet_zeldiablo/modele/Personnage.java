package modele;

import modele.Entite;
import modele.Labyrinthe;
import sprite.Sprites;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class Personnage implements Entite {
	/**
	 * nom du personnage
	 */
	private String nom;
	/**
	 * pv du personnage
	 */
	private int pv;
	/**
	 * abscisse du personnage
	 */
	private int posX;
	/**
	 * ordonnee du personnage
	 */
	private int posY;
	/**
	 * labyrinthe du personnage
	 */
	private Labyrinthe lab;
	/**
	 * niveau dans lequel se trouve le personnage
	 */
	private Niveau nPrinc;
	/**
	 * direction du personnage
	 */
	private String direction;
	/**
	 * liste dobjets du personnage
	 */
	private ArrayList<Item> objets;
	/**
	 * nombre damulettes du personnage, utile dans la conditon de victoire
	 */
	private int nbAmulettes;
	/**
	 * indique si le personnage attaque
	 */
	protected boolean attq;
	
	
	/**
	 * Constructeur de personnage avec un labyrinthe
	 */
	
	public Personnage(Labyrinthe l) {
		this.nom = "Steve";
		this.pv = 5;
		try {
			this.posX = l.getXe();
			this.posY = l.getYe();
		} catch (Exception e) {
			this.posX = 10;
			this.posY = 10;
		}
		this.lab=l;
		System.out.println("4lllllllllllllll4");
		System.out.println(lab);
		this.lab.ajouterEntite(this);
		this.lab.getCase(this.posX,this.posY).placerEntite(this);
		System.out.println("55lllllllllllllll");
		this.direction=("N");
		this.objets = new ArrayList<Item>();
	}
	
	/**
	 * constructeur de personnage
	 * @param n niveau lie a celui ci
	 */
	public Personnage(Niveau n) {
		this.nPrinc = n;
		this.nom = "Steve";
		this.pv = 5;
		try {
			this.posX = n.getLabEc().getXe();
			this.posY = n.getLabEc().getYe();
		} catch (Exception e) {
			this.posX = 0;
			this.posX = 0;
		}
		this.lab=nPrinc.getLabEc();
		System.out.println(lab);
		System.out.println(nPrinc.getLabEc());
		this.direction=("N");
		this.objets = new ArrayList<Item>();
		this.lab.ajouterEntite(this);
		this.lab.getCase(this.posX,this.posY).placerEntite(this);
		n.setPers(this);
	}
	
	/**
	 * Constructeur avec param�tre
	 * @param nomPers nom de Personnage
	 * @param vie vie de Personnage
	 * @param posX abscisse de la position de Personnage
	 * @param posY ordonn�e de la position de Personnage
	 * @param l labyrinthe dans lequel est le Personnage
	 */
	public Personnage(String nomPers, int vie, int posX, int posY, Labyrinthe l) {
		nom = nomPers;
		pv= vie;
		this.posX = posX;
		this.posY = posY;
		this.lab=l;
		this.lab.ajouterEntite(this);
		this.lab.getCase(this.posX,this.posY).placerEntite(this);
		this.direction=("N");
		this.objets = new ArrayList<Item>();
		this.direction = "N";
	}
	
	/**
	 * retourne le nom
	 * @return retourne sous forme de string le nom du monstre
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * retourne la vie
	 * @return retourne la vie du monstre sous la forme d'un enti�
	 */
	public int getPv() {
		return this.pv;
	}
	
	/**
	 * retourne l'abscisse du monstre
	 * @return l'abscisse sous la forme d'un enti�
	 */
	public int getPosX() {
		return this.posX;
	}
	/**
	 * retourne l'ordonn�e du monstre
	 * @return l'ordonn�e du monstre sous la forme d'un enti�
	 */
	public int getPosY() {
		return this.posY;
	}

	/**
	 * @return la direction du personnage
	 */
	@Override
	public String getDirection() {
		return direction;
	}

	/**
	 * @return le labyrinthe du personnage
	 */
	public Labyrinthe getLab() {
		return lab;
	}
	/**
	 * gette
	 * @return liste ditem du personnage
	 */
	public ArrayList<Item> getItems(){
		return this.objets;
	}

	/**
	 * methode qui permet en fonction de la touche appuy� de ce d�placer
	 * @param s touche appuy�
	 */
	public void seDeplacer(String s) {
		switch (s){
		case "N":

			direction = "N";
			this.direction="N";
			if(this.posY!=this.getLab().getTailleY()-1) {
				if (this.lab.getCase(this.posX, this.posY + 1).isTraversable() && ! this.lab.getCase(this.posX, this.posY + 1).isOccupe()) {
					this.lab.getCase(this.posX, this.posY).retirerEntite();
					this.posY += 1;
					this.lab.getCase(this.posX, this.posY).placerEntite(this);
					
				}
			}
			break;
		case "E":
			this.direction="E";
			if(this.posX!=this.getLab().getTailleX()-1) {
				if (this.lab.getCase(this.posX + 1, this.posY).isTraversable() && ! this.lab.getCase(this.posX + 1, this.posY).isOccupe()) {
					this.lab.getCase(this.posX, this.posY).retirerEntite();
					this.posX += 1;
					this.lab.getCase(this.posX, this.posY).placerEntite(this);
				}
				break;
			}
		case "S":
			this.direction="S";
			if(this.posY!=0) {
				if (this.lab.getCase(this.posX, this.posY - 1).isTraversable() && ! this.lab.getCase(this.posX, this.posY - 1).isOccupe()) {
					this.lab.getCase(this.posX, this.posY).retirerEntite();
					this.posY -= 1;
					this.lab.getCase(this.posX, this.posY).placerEntite(this);
				}
			}
			break;
		case "O":
			this.direction="O";
			if(this.posX!=0) {
				if (this.lab.getCase(this.posX - 1, this.posY).isTraversable() && ! this.lab.getCase(this.posX - 1, this.posY).isOccupe()) {
					this.lab.getCase(this.posX, this.posY).retirerEntite();
					this.posX -= 1;
					this.lab.getCase(this.posX, this.posY).placerEntite(this);
				}
			}
				break;
		}
	}
	/**
	 * retourne la distance entre deux entites
	 */
	public int distanceEntre() {
		int res = 1000;

		ArrayList<Entite> e =lab.getEntitesLab();
		for (int i=0; i<e.size();i++) {
			int temp = (int) (Math.sqrt(Math.pow(this.getPosX()-e.get(i).getPosX(),2) + Math.pow(this.getPosY()-e.get(i).getPosY(),2)));
		if (temp<res) {
			res=temp;
		}
		}
		return res;
	}
	
	/**
	 * retourne l'�tat du personnage
	 * @return �tat du personnage
	 */
	public boolean estMort() {
		if (this.getPv() <= 0) {
		// git 	this.lab.getEntitesLab().remove(this);
			return true;
		} else { 
			return false;
		}
	}
	/**
	 * methode qui permet de recevoir les d�gats des monstre
	 */
	public void prendreDegat() {
		this.pv= pv-1;
	}
	/**
	 * indique si deux  entites sont a portee
	 * @param i
	 * @return
	 */
	public boolean aPorter(int i) {
        boolean res=false;
        int index = i;
        ArrayList<Entite> e =lab.getEntitesLab();

        if (e.get(index).getPosX() == this.getPosX()+1 && e.get(index).getPosY() == this.getPosY()) {
        res = true;
    } else {
        if (e.get(index).getPosX() == this.getPosX()-1 && e.get(index).getPosY() == this.getPosY()) {
        res = true;
    } else {
        if (e.get(index).getPosX() == this.getPosX() && e.get(index).getPosY() == this.getPosY()+1) {
        res = true;
    } else { 
        if (e.get(index).getPosX() == this.getPosX() && e.get(index).getPosY() == this.getPosY()-1) {
        res = true;
    }
   }
  }
 }
        return res;
}
    /**
     * methode qui permet d'attaquer un monstre
     */
    public void attaquer() {
        ArrayList<Entite> e =lab.getEntitesLab();
        System.out.println("attaque");
        for (int i=0; i< e.size();i++) {
            if (e.get(i) instanceof Monstre) {
                if (aPorter(i)) {
                e.get(i).prendreDegat();
                } 
            }
        }
    }
/**
 *  induit une attaque a distance
 */
	public void attaqueDistance(){
		Projectile fleche = new Projectile(this,"fleche");
	}
	/**
	 * attribue une position aleatoire
	 */
	public void setRandPos() {
		this.posX = (int)(Math.random()*(lab.getTailleX()));
		this.posY = (int)(Math.random()*(lab.getTailleY()));
	}
	
	
	/**
	 * methode permettant de rammasser un item au sol si il sagit dune amulette, on gange 1 pv
	 * @param i item a rammasser
	 */
	public void ammasserItem(Item i) {
		if((this.getPosX() == i.getPosX()) && (this.getPosY() == i.getPosY())) {
			System.out.println("test");
			i.setPorteur(this);
			this.objets.add(i);
			if(i instanceof Amulette)
				this.pv++;
			
			lab.retirerItem(i);
			
		}
	}
	/**
	 * methode daffichage graphique
	 */
	public void afficher(Graphics g, int px, int py) {
		if (direction == "N")
			Sprites.dessiner(g, "hero_0_2", px, py);
		if (direction == "S")
			Sprites.dessiner(g, "hero_0_0", px, py);
		if (direction == "E")
			Sprites.dessiner(g, "hero_0_3", px, py);
		if (direction == "O")
			Sprites.dessiner(g, "hero_0_1", px, py);
		g.setColor(new Color(0,0,0));
		g.fillRect(px+8, py-10, 50, 5);
		g.setColor(new Color(128,16,16));
		g.fillRect(px+8, py-10, 10*this.pv, 5);
	}
/**
 * inflige les degats dun piege
 */
	@Override
	public void prendreDegatPiege() {
		this.pv -= 4;
		
	}
	/**
	 * permet de prendre une echelle
	 */
	public void prendreEchelle(){
		this.nPrinc.labSuivant();
	}
	/**
	 * setter
	 * @param l labyrinthe
	 */
	public void setLab(Labyrinthe l) {
		this.lab = l;
	}

	


}

