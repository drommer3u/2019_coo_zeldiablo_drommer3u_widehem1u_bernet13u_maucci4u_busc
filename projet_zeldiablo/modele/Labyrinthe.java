package modele;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Labyrinthe {
	/**
	 * parametre tableau de cases, represente le labyrinthe
	 */
    private Case[][] cases;
    /**
     * liste dentite presentent dans le labyrinthe
     */
    private ArrayList<Entite> entitesLab;
    /**
     * liste ditems present dans le labyrinthe
     */
    private ArrayList<Item> itemLab;
    /**
     * taille du labyrinthe en abscisse
     */
    private int tailleX;
    /**
     * taille du labyrinthe en ordonnee
     */
    private int tailleY;
    /**
     * niveau contenant ce labyrinthe
     */
    private Niveau niveau;
    /**
     * position X de l'entr�e
     */
    private int posXe;
    /**
     * position Y de l'entr�e
     */
    private int posYe;

    /**
     * constructeur de labyrinthe, y associe un niveau
     * @param n
     */
    public Labyrinthe(Niveau n){
        this.tailleX=10;
        this.tailleY=10;
        this.cases = new Case[this.tailleX][this.tailleY];
        for(int i =0;i<this.tailleX;i++){
            for(int j=0;j<this.tailleY;j++){
                if((j==0||i==0||j==9||i==9) && (i!=5||j==9)){
                    this.cases[i][j]=new Case(false);
                }
                else{
                    this.cases[i][j]= new Case(true);
                }
            }
        }
        this.posXe = 5;
        this.posYe = 0;
        this.entitesLab=new ArrayList<Entite>();
        this.itemLab = new ArrayList<Item>();
        this.niveau=n;
        n.ajouterLab(this);
        //new Amulette("test", this);
    }
    /**
     * constructeur de niveau vide
     */
    public Labyrinthe(){
        this.tailleX=10;
        this.tailleY=10;
        this.cases = new Case[this.tailleX][this.tailleY];
        for(int i =0;i<this.tailleX;i++){
            for(int j=0;j<this.tailleY;j++){
                if((j==0||i==0||j==9||i==9) && (i!=5||j==9)){
                    this.cases[i][j]=new Case(false);
                }
                else{
                    this.cases[i][j]= new Case(true);
                }
            }
        }
        this.posXe = 5;
        this.posYe = 0;
        this.entitesLab=new ArrayList<Entite>();
        this.itemLab = new ArrayList<Item>();
        //new Amulette("test", this);
    }
    /**
     * constructeur labyrinthe, lit un fichier et le transforme en labyrinthe
     * @param fich
     */
    public Labyrinthe(String fich){
        String chaine = "";
        int i=0;
        try(
                InputStream ips = new FileInputStream("projet_zeldiablo\\labyrintheTexte\\"+fich);
                InputStreamReader ipsr = new InputStreamReader(ips);
                BufferedReader br = new BufferedReader(ipsr)) {
            String ligne;
            while ((ligne = br.readLine()) != null) {
                i+=1;
                chaine += ligne;
                this.tailleX=ligne.length();
            }
            this.tailleY=i;
        }catch (Exception e){
            e.printStackTrace();
        }
        this.cases = new Case[this.tailleX][this.tailleY];
        for (int j=0;j<tailleY;j++){
            for(int k=0;k<tailleX;k++){
                cases[k][tailleY-1-j]=traduireCase(Integer.parseInt(String.valueOf(chaine.charAt(j*tailleX+k))));
            }
        }
        this.entitesLab=new ArrayList<Entite>();
        this.itemLab = new ArrayList<Item>();
        this.posYe=0;
        this.posXe=0;
        new Amulette("am", this);
    }

    /**
     * methode utile dans la creation de labyrinthe a partir de fichiers
     * @param i permet de verifier si la valeur correspond a un mur, une case vide ....
     * @return retourne une case
     */
    public Case traduireCase(int i){
        Case c=null;
        switch(i){
            //sol traversable
            case 0:
                c=new Case(true);
                break;
            //mur
            case 1:
                c=new Case(false);
                break;
            case 2:
                c=new CaseActivable(true);
                break;
            case 3:
                c=new CaseActivable(false);
                break;
        }
        return c;

    }
    
    /**
     * attribue une valeur au tableau de cases
     */
    public void setLab1() {
    	this.cases = new Case[][] {{new Case(true)},{new Case(true)}};
    }
    
    
    /**
     * permet dajouter un item au labyrinthe
     * @param i item a ajouter
     * @param x position abscisse
     * @param y position ordonnee
     */
    public void ajouterItem(Item i, int x, int y) {
    	System.out.println("47");
        this.itemLab.add(i);
        boolean succes = false;
        while (!succes) {
	        if (ajPossible(i.getPosX(), i.getPosY())) {
	        	this.cases[i.getPosX()][i.getPosY()].placerItem(i);
	        	succes = true;
	        } else {
	        	i.setRandPos();
	        	System.out.println(i.getPosX());
	        	System.out.println(i.getPosY());
	        	
	        }
        }
    }
    /**
     * permet leretrait dun item
     * @param i item a retirer
     */
    public void retirerItem(Item i) {
    	cases[i.getPosX()][i.getPosY()].retirerItem();
    	itemLab.remove(i);
    }
    /**
     * getter tableau cases
     * @return tableau cases
     */
    public Case[][] getCases() {
        return cases;
    }
    /**
     * getter
     * @return taille abscisse
    */
    public int getTailleX() {
        return tailleX;
    }

    /**
     * setter
     * @param niveau niveau contenant le labyrinthe
     */
    public void setNiveau(Niveau niveau) {
        this.niveau = niveau;
    }
    /**
     * getter
     * @return taille ordonne du labyrinthe
     */

    public int getTailleY() {
        return tailleY;
    }
    /**
     * getter case
     * @param x abscisse
     * @param y ordonnee
     * @return case 
     */
    public Case getCase(int x, int y) {
    	return cases[x][y];
    }
    /**
     * getter
     * @return niveau
     */
    public Niveau getNiveau() {
        return niveau;
    }
    /**
     * permet dajouter une entite au labyrinthe
     * @param e
     */
    public void ajouterEntite(Entite e){
    	System.out.println("47");
        this.entitesLab.add(e);
        boolean succes = false;
        while (!succes) {
	        if (ajPossible(e.getPosX(), e.getPosY())) {
	        	this.cases[e.getPosX()][e.getPosY()].placerEntite(e);
	        	succes = true;
	        } else {
	        	e.setRandPos();
	        	System.out.println(e.getPosX());
	        	System.out.println(e.getPosY());
	        	
	        }
        }
    }
    /**
     * getter
     * @return liste entite du labyrinthe
     */
    public ArrayList<Entite> getEntitesLab() {
        return entitesLab;
    }
    /**
     * getter
     * @return liste item du labyrinthe
     */
    public ArrayList<Item> getItemLab(){
    	return itemLab;
    }
    /**
     * getter
     * @return personnage attribue au labyrinthe
     */
    public Personnage getPers() {
    	for (int i = 0; i < entitesLab.size(); i++) {
    		if(entitesLab.get(i) instanceof Personnage) {
    			return (Personnage)entitesLab.get(i);
    		}
    	}
    	return null;
    }
    
    /**
     * m�thode d�clarant si l'ajout d'un(e) entit�/item est possible � la position donn�e
     * @param x int 
     * @param y
     * @return ajout possible ou non
     */
    public boolean ajPossible(int x, int y) {
    	if (cases[x][y].isTraversable() && !cases[x][y].isOccupe()) {
    		return true;
    	}
    	return false;
    }
    
    /**
     * M�thode retournant la case X la plus proche si la case initial n'est pas disponible
     * @return Position la plus proche, ou elle meme si elle est disponible
     */
    /*public int procheX(int x, int y) {
    	if (ajPossible(x, y)) {
    		return x;
    	} else {
    		
    	}
    }
    
    public int procheY(int x, int y) {
    	
    }*/
    
    /**
     * getter 
     * @return position entree labyrinthe abscisse
     */
    public int getXe() {
    	return this.posXe;
    }
    /**
     * getter
     * @return ordonne entree labyrinthe
     */
    public int getYe() {
    	return this.posYe;
    }
    /**
     * utilisee dans la methode evoluer, permet de repeter des actions en permanence
     */
    public void refresh(){

    	checkEntite();
    	checkItem();
    	checkAmulette();

        for(int i=0;i<this.entitesLab.size();i++){
            if((this.entitesLab.get(i) instanceof Monstre)){
            	if (this.entitesLab.get(i) instanceof Fantome || this.entitesLab.get(i) instanceof Boss) {
            		((Monstre)entitesLab.get(i)).iaAmeliorer();
            	} else {
            		((Monstre)entitesLab.get(i)).Ia();
            	}
            		
            }
        }
        for(int j=0;j<this.tailleX;j++){
            for(int k=0;k<this.tailleY;k++){
                if(this.getCase(j,k).getProjectileCase()!=null){
                    this.getCase(j,k).getProjectileCase().seDeplacer();
                }
                else{if(this.getCase(j,k).getItemCase()!=null && this.getPers()!=null){
                    this.getPers().ammasserItem(itemLab.get(0));
                    }
                }
            }
        }
    }
    /**
     * permet de retirer une entite du labyrinthe
     * @param e entite a retirer
     */
	public void retirerEntite(Entite e) {
		cases[e.getPosX()][e.getPosY()].retirerEntite();
		entitesLab.remove(e);
		
	}
	/**
	 * utilisee dans la verification de la mort des entite, les retire en cas de mort
	 */
	public void checkEntite() {
		for (int i=0; i < entitesLab.size();i++) {
			if (entitesLab.get(i).estMort()&&entitesLab.get(i)!=getPers()) {
				retirerEntite(entitesLab.get(i));

			}

		}	
	}
	/**
	 * utile dans le rammassage ditem, les retire du labyrinthe
	 */
	public void checkItem() {
		for (int i = 0; i < itemLab.size();i++) {
			if (itemLab.get(i).getPosX() == this.getPers().getPosX() && itemLab.get(i).getPosY() == this.getPers().getPosY()) {
				getPers().ammasserItem(itemLab.get(i));
				System.out.println(getPers().getPv());
				System.out.println(getPers().getItems());
			}
		}
	}
	
/**
 * permet dajouter une condition de victoire
 * @return
 */
	public boolean checkAmulette() {
		boolean res = false;
		int nb = 0;
		for (int i = 0; i<this.getPers().getItems().size(); i++) {
			if(this.getPers().getItems().get(i) instanceof Amulette) {
				nb++;
			}
		}
		if(nb == this.niveau.getLabs().size()) {
			this.niveau.setFin();
		}
		return res;
	}
	/**
	 * methode toString du tableau de cases
	 */
	public String toString() {
		return cases.toString();
				
	}

}