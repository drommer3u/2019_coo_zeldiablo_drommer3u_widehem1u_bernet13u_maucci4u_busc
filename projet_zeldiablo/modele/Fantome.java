package modele;
import modele.Entite;
import modele.Labyrinthe;
import sprite.Sprites;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class Fantome extends Monstre {
	/**
	 * Constructeur de Fantome prenant en parametre un labyrinthe
	 */
	public Fantome(Labyrinthe l) {
		super(l);
	}
	
	/**
	 * Constructeur avec parametre
	 * @param n nom du Fantome
	 * @param vie vie du Fantome
	 * @param posX abscisse de la position du Fantome
	 * @param posY ordonnee de la position du Fantome
	 */
	public Fantome(String n, int vie, int posX, int posY, Labyrinthe l) {
		super(n,vie,posX,posY,l);
	}

	/**
	 * methode de deplacement du fantome, il traverse les murs
	 */
	public void seDeplacer(String s) {
		switch (s){
			case "N":
				if(this.posY!=this.getLab().getTailleY()-1) {
					if (! this.lab.getCase(this.posX, this.posY + 1).isOccupe()) {
						this.lab.getCase(this.posX, this.posY).retirerEntite();
						this.posY += 1;
						this.lab.getCase(this.posX, this.posY).placerEntite(this);
					}
				}
				break;
			case "E":
				if(this.posX!=this.getLab().getTailleX()-1) {
					if (! this.lab.getCase(this.posX + 1, this.posY).isOccupe()) {
						this.lab.getCase(this.posX, this.posY).retirerEntite();
						this.posX += 1;
						this.lab.getCase(this.posX, this.posY).placerEntite(this);
					}
					break;
				}
			case "S":
				if(this.posY!=0) {
					if (! this.lab.getCase(this.posX, this.posY - 1).isOccupe()) {
						this.lab.getCase(this.posX, this.posY).retirerEntite();
						this.posY -= 1;
						this.lab.getCase(this.posX, this.posY).placerEntite(this);
					}
				}
				break;
			case "O":
				if(this.posX!=0) {
					if (! this.lab.getCase(this.posX - 1, this.posY).isOccupe()) {
						this.lab.getCase(this.posX, this.posY).retirerEntite();
						this.posX -= 1;
						this.lab.getCase(this.posX, this.posY).placerEntite(this);
					}
				}
				break;
		}
		this.direction=s;
	}
	
	/**
	 * methode daffichage graphique, ajoute les sprites
	 */
	public void afficher(Graphics g, int px, int py) {
		if (direction == "N")
			Sprites.dessiner(g, "ghost_2_0", px, py);
		if (direction == "S")
			Sprites.dessiner(g, "ghost_0_0", px, py);
		if (direction == "E")
			Sprites.dessiner(g, "ghost_3_0", px, py);
		if (direction == "O")
			Sprites.dessiner(g, "ghost_1_0", px, py);
		g.setColor(new Color(0,0,0));
		g.fillRect(px+8, py-10, 50, 5);
		g.setColor(new Color(16,128,16));
		g.fillRect(px+8, py-10, 10*this.pv, 5);
	}
	

	
}

