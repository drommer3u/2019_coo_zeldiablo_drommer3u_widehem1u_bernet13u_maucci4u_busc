package modele;

import java.util.ArrayList;

import moteurJeu.Chrono;

public class Troll extends Monstre{
	/**
	 * vie du troll
	 */
	private int pvMax;
	/**
	 * indique si le troll est blesse, permet la regeneration
	 */
	private boolean estBlesser;
	/**
	 * chronometrage des actions du troll
	 */
	private Chrono cActionR;
	
	/**
	 * Constructeur vide de Troll
	 */
	public Troll(Labyrinthe l) {
	    super(l);
		this.pvMax = 5;
		this.estBlesser = false;
		setChronoR(2);
		
	}
	
	/**
	 * Constructeur avec parametre
	 * @param n nom du Troll
	 * @param vie vie du Troll
	 * @param posX abscisse de la position du Troll
	 * @param posY ordonnee de la position du Troll
	 */
	public Troll(String n, int vie, int posX, int posY, Labyrinthe l) {
		super(n,vie,posX,posY,l);
		this.estBlesser = false;
		this.pvMax=vie;
		setChronoR(2);
		
	}	
	/**
	 * indique la frequence d'action du troll
	 * @param t frequence
	 */
	public void setChronoR(double t) {
		this.cActionR = new Chrono(t);
	}

	/**
	 * methode qui permet de recevoir les degats d'un personnage
	 */
	public void prendreDegat() {
		this.pv= pv-1;
		this.estBlesser = true;
	}
	
	
/**
 * methode permettant au Monstre de ce deplacer
 * @param s direction du déplacements
 */
	public void seDeplacer(String s) {
		super.seDeplacer(s);

	}
	/**
	 * ia permettant mouvement aleatoire
	 */
	public void Ia() {
		super.Ia();
		if (cActionR.check()) {
			this.regenerer();
		}
	}

	
	/**
	 * methode permettant au Troll de se soigner
	 */
	public void regenerer() {
		if (pv < pvMax)
			this.pv += 1;
	}
	

}
