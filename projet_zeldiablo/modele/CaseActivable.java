package modele;

import sprite.Sprites;

import java.awt.*;

public class CaseActivable extends Case {
	/**
	 * booleen indiquant si la case est une échelle
	 */
   private boolean echelle;
	
   /**
    * constructeur caseActivable
    * @param echelle indique si la case est une echelle ou non
    */
	public CaseActivable(boolean echelle) {
		super(true);
		this.echelle=echelle;
		
	}
	/**
	 * permet dactiver un piege ou une echelle
	 */
	public void activer() {
		if(!echelle) {
			this.entitesCase.prendreDegatPiege();
		}
		else{
			if(this.entitesCase instanceof Personnage)
				((Personnage)this.entitesCase).prendreEchelle();
		}
	}
	/**
	 * permet de placer une entite sur la case et de lactiver
	 */
	@Override
	public void placerEntite(Entite e) {
		super.placerEntite(e);
		this.activer();
	}

	/**
	 * verifie si la case comporte une echelle
	 * @return boolean echelle
	 */
	public boolean isEchelle() {
		return echelle;
	}

	/**
	 * methode daffichage graphique
	 */
	public void affiche(Graphics g, int px, int py) {
		if (traversable) {
			Sprites.dessiner(g, "sol", px, py);
			if(echelle) {
				Sprites.dessiner(g, "escalier_2_0", px, py);
			}
			else {
				Sprites.dessiner(g, "piege2", px, py);
			}

		} else if (!traversable){
			Sprites.dessiner(g, "mur", px, py);
		} if(estOccupe && this.entitesCase != null) {
			this.entitesCase.afficher(g, px, py);
		} else {
			if(estOccupe && this.projectileCase != null) {
				projectileCase.afficher(g, px, py);

				//this.entitesCase.afficher();
			} else {
				if (this.entitesCase != null) {
					System.out.println("ici");
					this.entitesCase.afficher(g, px, py);
				}
			}
		}
	}

}
