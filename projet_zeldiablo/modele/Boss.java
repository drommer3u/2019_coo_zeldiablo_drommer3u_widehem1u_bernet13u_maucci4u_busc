package modele;

import java.awt.Color;
import java.awt.Graphics;

import sprite.Sprites;

public class Boss extends Monstre {

	public Boss(Labyrinthe l) {
		super(l);
		this.pv = 15;
		// TODO Auto-generated constructor stub
	}
	
	public void afficher(Graphics g, int px, int py) {
		if (direction == "N")
			Sprites.dessiner(g, "boss_0_3", px, py);
		if (direction == "S")
			Sprites.dessiner(g, "boss_0_0", px, py);
		if (direction == "E")
			Sprites.dessiner(g, "boss_0_2", px, py);
		if (direction == "O")
			Sprites.dessiner(g, "boss_0_1", px, py);
		g.setColor(Color.black);
		g.fillRect(px-40, py-10, 150, 5);
		g.setColor(Color.yellow);
		g.fillRect(px-40, py-10, 10*this.pv, 5);
	}

}
