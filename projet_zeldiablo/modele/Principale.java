package modele;

import moteurJeu.Chrono;
import moteurJeu.Dessin;
import moteurJeu.Jeu;
import moteurJeu.MoteurGraphique;
import moteurJeu.Musique;

public class Principale {

	public static void main(String[] args) {
		
		Niveau n = new Niveau("test");
		Dessin tdes = new Dessin(n);
		Jeu tjeu = new Jeu(tdes,n);
		Musique.lancerMusique("themezel");
		n.setJeu(tjeu);
		n.setDess(tdes);
		Personnage p = new Personnage(n);
		MoteurGraphique mt = new MoteurGraphique(tjeu, tdes);

		mt.lancerJeu(1920, 1080, 50);



	}

}
