package modele;

import java.awt.Graphics;

import moteurJeu.Chrono;
import sprite.Sprites;

public class Projectile {
	/**
	 * ascisse du projectile
	 */
	private int posX;
	/**
	 * ordonnee du projectile
	 */
    private int posY;
    /**
     * direction du projectile
     */
    private String direction;
    /**
     * entite 
     */
    private Entite entite;
    /**
     * sprite du projectile
     */
    private String sprite;
    /**
     * chronometrage du projectile
     */
    private Chrono cAction;

    /**
     * constructeur
     * @param e entite source du projectile
     * @param sprite 
     */
    public Projectile(Entite e,String sprite){
    	this.cAction = new Chrono(0.075);
        switch(e.getDirection()){
            case "N":
                this.posX=e.getPosX();
                this.posY=e.getPosY()+1;
                break;
            case "S":
                this.posX=e.getPosX();
                this.posY=e.getPosY()-1;
                break;
            case "E":
                this.posX=e.getPosX()+1;
                this.posY=e.getPosY();
                break;
            case "O":
                this.posX=e.getPosX()-1;
                this.posY=e.getPosY();
                break;
        }
        this.direction=e.getDirection();
        this.sprite=sprite;
        this.entite=e;
        this.entite.getLab().getCase(this.posX,this.posY).placerProjectile(this);
    }
    /**
     * deplacement du projectile
     */
    public void seDeplacer(){
    	if (cAction.check())
        switch (this.direction){
            case "N":
                    if(this.posY!=this.entite.getLab().getTailleY()-1) {
                        if (this.entite.getLab().getCase(this.posX, this.posY + 1).isTraversable() && ! this.entite.getLab().getCase(this.posX, this.posY + 1).isOccupe()) {
                            this.entite.getLab().getCase(this.posX, this.posY).retirerProjectile();
                            this.posY += 1;
                            this.entite.getLab().getCase(this.posX, this.posY).placerProjectile(this);
                        } else {
                            if (! this.entite.getLab().getCase(this.posX, this.posY + 1).isTraversable()) {
                                this.entite.getLab().getCase(this.posX, this.posY).retirerProjectile();
                                break;
                            } else {
                                this.entite.getLab().getCase(this.posX, this.posY + 1).getEntitesCase().prendreDegat();
                                this.entite.getLab().getCase(this.posX, this.posY).retirerProjectile();
                                break;
                            }
                        }
                    }
                break;
            case "E":
                    if(this.posX!=this.entite.getLab().getTailleX()-1) {
                        if (this.entite.getLab().getCase(this.posX + 1, this.posY).isTraversable() && ! this.entite.getLab().getCase(this.posX + 1, this.posY).isOccupe()) {
                            this.entite.getLab().getCase(this.posX, this.posY).retirerProjectile();
                            this.posX += 1;
                            this.entite.getLab().getCase(this.posX, this.posY).placerProjectile(this);
                        } else {
                            if (! this.entite.getLab().getCase(this.posX + 1, this.posY).isTraversable()) {
                                this.entite.getLab().getCase(this.posX, this.posY).retirerProjectile();
                                break;
                            } else {
                                this.entite.getLab().getCase(this.posX + 1, this.posY).getEntitesCase().prendreDegat();
                                this.entite.getLab().getCase(this.posX, this.posY).retirerProjectile();
                                break;
                            }
                        }
                    }
                
                break;
            case "S":
                    if(this.posY!=0) {
                        if (this.entite.getLab().getCase(this.posX, this.posY - 1).isTraversable() && ! this.entite.getLab().getCase(this.posX, this.posY - 1).isOccupe()) {
                            this.entite.getLab().getCase(this.posX, this.posY).retirerProjectile();
                            this.posY -= 1;
                            this.entite.getLab().getCase(this.posX, this.posY).placerProjectile(this);
                        } else {
                            if (! this.entite.getLab().getCase(this.posX, this.posY - 1).isTraversable()) {
                                this.entite.getLab().getCase(this.posX, this.posY).retirerProjectile();
                                break;
                            } else {
                                this.entite.getLab().getCase(this.posX, this.posY - 1).getEntitesCase().prendreDegat();
                                this.entite.getLab().getCase(this.posX, this.posY).retirerProjectile();
                                break;
                            }
                        }
                    }
                break;
            case "O":
                    if(this.posX!=0) {
                        if (this.entite.getLab().getCase(this.posX - 1, this.posY).isTraversable() && ! this.entite.getLab().getCase(this.posX - 1, this.posY).isOccupe()) {
                            this.entite.getLab().getCase(this.posX, this.posY).retirerProjectile();
                            this.posX -= 1;
                            this.entite.getLab().getCase(this.posX, this.posY).placerProjectile(this);
                        } else {
                            if (! this.entite.getLab().getCase(this.posX - 1, this.posY).isTraversable()) {
                                this.entite.getLab().getCase(this.posX, this.posY).retirerProjectile();
                                break;
                            } else {
                                this.entite.getLab().getCase(this.posX - 1, this.posY).getEntitesCase().prendreDegat();
                                this.entite.getLab().getCase(this.posX, this.posY).retirerProjectile();
                                break;
                            }
                        }
                    }
                break;
        }

    }
    /**
     * getter
     * @return directione du projectile
     */
    public String getDirection() {
        return direction;
    }
    /**
     * getter
     * @return entite source du tir
     */
    public Entite getE() {
        return entite;
    }
    /**
     * position abscisse du projectile
     * @return
     */
    public int getPosX() {
        return posX;
    }
    /**
     * getter
     * @return ordonne du projectile
     */
    public int getPosY() {
        return posY;
    }
/**
 * getter
 * @return sprite du projectile
 */
    public String getSprite() {
        return sprite;
    }
    /**
     * methode d'affichage graphique
     * @param g parametre graphique
     * @param px abscisse
     * @param py ordonnee
     */
    public void afficher(Graphics g, int px, int py) {
    	switch(this.direction) {
    	case "N" :
    		Sprites.dessiner(g, "arrow_0_0", px, py);
    		break;
    	case "S" :
    		Sprites.dessiner(g, "arrow_2_0", px, py);
    		break;
    	case "E" :
    		Sprites.dessiner(g, "arrow_3_0", px, py);
    		break;
    	case "O" :
    		Sprites.dessiner(g, "arrow_1_0", px, py);
    		break;
    		
    	}
    }

}



