package modele;

import java.awt.Graphics;

public interface Item {
	
	public String getNom();
	public int getPosX();
	public int getPosY();
	public Labyrinthe getLab();
	public Personnage getPorteur();
	public void setPorteur(Personnage p);
	public void affiche(Graphics g, int x, int y);
	public void setRandPos();
	
}
